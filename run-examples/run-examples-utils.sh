#!/bin/bash

export GREEN=$(tput setaf 2)
export BLUE=$(tput setaf 4)
export CYAN=$(tput setaf 6)
export NORMAL=$(tput sgr0)

balanceUser() {
    printf "\n${GREEN}$1${NORMAL}\n"
    ./balance.sh $1
}

stakeBalanceUser() {
    printf "\n${GREEN}$1${NORMAL}\n"
    ./stakeBalance.sh $1
}

printfWithColor() {
    printf "\n${GREEN}$1${NORMAL}\n"
}

printfPlutusCode() {
    printf "\n${CYAN}$1${NORMAL}\n"
}

sleep2() {
    sleep 2
}

pressContinue() {
    read -p "${BLUE}Press any key to continue...${NORMAL}"
}