#!/bin/bash

. ./run-examples-utils.sh

cd ../scripts
. ./env.sh

printfWithColor "First we create two sets of keys and addresses, let's call them user1 and user2."
./create-user.sh user1
./create-user.sh user2
sleep2 

printfWithColor "Now we need to fund user1."
./fund-user.sh user1
printfWithColor "His address is: $(cat $WALLET_PATH/user1.addr)"
printfPlutusCode "$(cabal run read-address $(cat $WALLET_PATH/user1.addr))"
sleep 5
balanceUser user1

printfWithColor "We don't fund user2."
printfWithColor "His address is: $(cat $WALLET_PATH/user2.addr)"
printfPlutusCode "$(cabal run read-address $(cat $WALLET_PATH/user2.addr))"

balanceUser user2
printfWithColor "As you see user1 now has 1000 Ada and user2 nothing."
pressContinue

printfWithColor "For now user1 has not staked."
printfWithColor "Choose a stake pool: "

./query-stake-pools.sh > $NETWORK_DIR_PATH/pools

cat $NETWORK_DIR_PATH/pools

read -p " Choose one of the pools: " pool

poolid=`sed -n $pool\p $NETWORK_DIR_PATH/pools`

printfWithColor "Now let's delegate through a contract to that pool."
printfPlutusCode "StakingValidator :: Address -> () -> ScriptContext -> Bool"
printfWithColor "A validator with an address as a parameter and the unit type as redeemer."
printfWithColor "We use the address of user2 as the parameter."
./register-and-delegate.sh user1 user2 $poolid
sleep2
sleep2

####
printfWithColor "We have now staked to a pool and moved our funds to our new payment address belonging to our stake address."
printfPlutusCode "$(cabal run read-address $(cat $WALLET_PATH/user1-script.addr))"
printfWithColor "Where the StakingCredential is the hash of our staking validator."

./query-stake-address-info-user1-script.sh user1

printfWithColor "No rewards."
printfWithColor "We need to wait around 3 minutes for our first rewards."
printfWithColor "Do not stop simulation!"

sleep 181

./query-stake-address-info-user1-script.sh user1

printfWithColor "There we have some rewards."
printfWithColor "Let us unlock the rewards. The way our staking validator is written we have to send them to user2."
printfWithColor "Current balance: "

balanceUser user1-script

sleep2

./withdraw-user1-script.sh user1-script
sleep 5
printfWithColor "New current balance: "

balanceUser user1-script

printfWithColor "user2 now also is funded"

balanceUser user2
