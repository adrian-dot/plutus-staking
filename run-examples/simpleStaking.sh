#!/bin/bash

. ./run-examples-utils.sh

cd ../scripts
. ./env.sh

printfWithColor "First we create a new set of keys and an address, let's call it user1."
./create-user.sh user1
sleep2
printfWithColor "user1"
cat $WALLET_PATH/user1.addr
printfWithColor "In plutus it looks like: "
printfPlutusCode "$(cabal run read-address $(cat $WALLET_PATH/user1.addr))"
printfWithColor "A public key hash and a staking public key which is nothing for now."
printfWithColor "Now we need to fund user1."
./fund-user.sh user1

sleep 5

balanceUser user1
printfWithColor "As you see user1 now has 1000 Ada."
pressContinue

printfWithColor "For now user1 has not staked. He does not have a stake key or stake address. So let's create both."

./create-stake-keys-and-new-payment-address.sh user1

printfWithColor "Our new address looks like: "
cat $WALLET_PATH/user1-payment.addr
printfWithColor "That's the address to receive funds."
cat $WALLET_PATH/user1-stake.addr
printfWithColor "Here we will receive our rewards."
printfWithColor "In plutus it looks like: "
printfPlutusCode "$(cabal run read-address $(cat $WALLET_PATH/user1-payment.addr))"
printfWithColor "A public key hash and a staking public key."

sleep2

printfWithColor "Now let's stake to stake pool."

./query-stake-pools.sh > $NETWORK_DIR_PATH/pools

cat $NETWORK_DIR_PATH/pools

read -p " Choose one of the pools: " pool

poolid=`sed -n $pool\p $NETWORK_DIR_PATH/pools`

printfWithColor "Now delegate to that pool."
./simple-register-and-delegate.sh user1 $poolid
sleep2
sleep2
printfWithColor "We have now staked to a pool and moved our funds to our new payment address with a staking part."

stakeBalanceUser user1

printfWithColor "No rewards."
printfWithColor "We need to wait around 3 minutes for our first rewards."
printfWithColor "Do not stop simulation!"

sleep 181

stakeBalanceUser user1

printfWithColor "There we have some rewards."
printfWithColor "Let's get that reward to our payment address."
printfWithColor "Current balance: "

cardano-cli query protocol-parameters --testnet-magic ${TESTNET_MAGIC} --out-file $PROTOCOL

balanceUser user1-payment

sleep2
sleep2

./withdraw-user.sh user1
sleep2
sleep2
sleep2
printfWithColor "New current balance: "

balanceUser user1-payment

















