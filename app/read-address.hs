{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

import System.Environment (getArgs)
import Text.Printf        (printf)
import Staking.Deploy      (tryReadAddress)
import PlutusTx.Builtins.Internal
import Ledger
import Data.String (fromString)

main :: IO ()
main = do
    [addr'] <- getArgs
    let Just addr = tryReadAddress addr'
    printf "addr: %s\n" (show addr)
