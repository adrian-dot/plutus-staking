import System.Environment (getArgs)
import Text.Printf        (printf)
import Staking.Deploy      (tryReadAddress, writeStakeValidator)

main :: IO ()
main = do
    [file, addr'] <- getArgs
    let Just addr = tryReadAddress addr'
    printf "file: %s\n" file
    e <- writeStakeValidator file addr
    case e of
        Left err -> print err
        Right () -> printf "wrote stake validator to %s\n" file
