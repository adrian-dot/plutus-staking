
##run examples
- `./000-setup.sh`
- start private testnet from your root directory
`./scripts/start-private-testnet.sh`

- open new terminal and enter your plutus-apps `nix develop` with tag `c4f4dc5fedd5b1804781abb7db0fb5a553e24ecb`

- run one of the examples provides in `run-examples`
    - `./simpleStaking` just staking without any plutus code involved
    - `./scriptStaking` staking through a plutus validator
