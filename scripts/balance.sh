#!/bin/bash

source env.sh

USER=$1

cardano-cli query utxo \
    --testnet-magic ${TESTNET_MAGIC} \
    --address $(cat $WALLET_PATH/$USER.addr)
