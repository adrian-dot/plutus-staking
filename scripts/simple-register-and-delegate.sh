#!/bin/bash

source env.sh

USER=$1
USER_ADDR=$(cat $WALLET_PATH/$USER.addr)

UTXO_IN=$(get_address_biggest_lovelace $USER_ADDR)

cardano-cli stake-address registration-certificate \
    --stake-verification-key-file $WALLET_PATH/$1-stake.vkey \
    --out-file $NETWORK_DIR_PATH/$1-registration.cert

cardano-cli stake-address delegation-certificate \
    --stake-verification-key-file $WALLET_PATH/$1-stake.vkey \
    --stake-pool-id $2 \
    --out-file $NETWORK_DIR_PATH/$1-delegation.cert 

cardano-cli query protocol-parameters \
    --testnet-magic ${TESTNET_MAGIC} \
    --out-file $PROTOCOL

cardano-cli transaction build \
    --testnet-magic ${TESTNET_MAGIC} \
    --change-address $(cat $WALLET_PATH/$1-payment.addr) \
    --tx-in ${UTXO_IN} \
    --tx-in-collateral ${UTXO_IN} \
    --certificate-file $NETWORK_DIR_PATH/$1-registration.cert \
    --certificate-file $NETWORK_DIR_PATH/$1-delegation.cert \
    --protocol-params-file $PROTOCOL \
    --out-file $TX_PATH/delegate.raw

cardano-cli transaction sign \
    --testnet-magic ${TESTNET_MAGIC} \
    --tx-body-file $TX_PATH/delegate.raw \
    --out-file $TX_PATH/delegate.signed \
    --signing-key-file $WALLET_PATH/$1-stake.skey  \
    --signing-key-file $WALLET_PATH/$1.skey

cardano-cli transaction submit \
    --testnet-magic ${TESTNET_MAGIC} \
    --tx-file $TX_PATH/delegate.signed
