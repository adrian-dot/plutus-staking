#!/bin/bash 

source env.sh

cardano-cli stake-address key-gen \
    --verification-key-file $WALLET_PATH/$1-stake.vkey \
    --signing-key-file $WALLET_PATH/$1-stake.skey

cardano-cli address build \
    --payment-verification-key-file $WALLET_PATH/$1.vkey \
    --stake-verification-key-file $WALLET_PATH/${1}-stake.vkey \
    --out-file $WALLET_PATH/${1}-payment.addr \
    --testnet-magic ${TESTNET_MAGIC}

cardano-cli stake-address build \
    --stake-verification-key-file $WALLET_PATH/${1}-stake.vkey \
    --out-file $WALLET_PATH/${1}-stake.addr \
    --testnet-magic ${TESTNET_MAGIC}
