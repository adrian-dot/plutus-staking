#!/bin/bash

source env.sh 


UTXO_IN=$(get_address_biggest_lovelace $(cat $WALLET_PATH/$1.addr)) 


script=$WALLET_PATH/stake-validator.script
script_stake_addr=$WALLET_PATH/$1-script-stake.addr
script_payment_addr=$WALLET_PATH/$1-script.addr
registration=$NETWORK_DIR_PATH/registration.cert
delegation=$NETWORK_DIR_PATH/delegation.cert
raw=$TX_PATH/tx.raw
signed=$TX_PATH/tx.signed


UTXO_IN=$(get_address_biggest_lovelace $(cat $WALLET_PATH/$1.addr))

cabal run write-unit-json 

cabal run write-stake-validator -- $script $(cat $WALLET_PATH/$2.addr)

cardano-cli stake-address build \
    --testnet-magic ${TESTNET_MAGIC} \
    --stake-script-file $script \
    --out-file $script_stake_addr

echo "stake address: $(cat $script_stake_addr)"

cardano-cli address build \
    --testnet-magic ${TESTNET_MAGIC} \
    --payment-verification-key-file $WALLET_PATH/$1.vkey \
    --stake-script-file=$script \
    --out-file $script_payment_addr

echo "payment address: $(cat $script_payment_addr)"

cardano-cli stake-address registration-certificate \
    --stake-script-file $script \
    --out-file $registration

cardano-cli stake-address delegation-certificate \
    --stake-script-file $script \
    --stake-pool-id $3 \
    --out-file $delegation 

cardano-cli query protocol-parameters \
    --testnet-magic ${TESTNET_MAGIC} \
    --out-file $PROTOCOL

cardano-cli transaction build \
    --testnet-magic ${TESTNET_MAGIC} \
    --change-address $(cat $script_payment_addr) \
    --out-file $raw \
    --tx-in ${UTXO_IN} \
    --tx-in-collateral ${UTXO_IN} \
    --certificate-file $registration \
    --certificate-file $delegation \
    --certificate-script-file $script \
    --certificate-redeemer-file $NETWORK_DIR_PATH/unit.json \
    --protocol-params-file $PROTOCOL

cardano-cli transaction sign \
    --testnet-magic ${TESTNET_MAGIC} \
    --tx-body-file $raw \
    --out-file $signed \
    --signing-key-file $WALLET_PATH/$1.skey

cardano-cli transaction submit \
    --testnet-magic ${TESTNET_MAGIC} \
    --tx-file $signed
