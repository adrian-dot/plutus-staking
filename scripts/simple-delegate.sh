#!/bin/bash

source env.sh

USER=$1
USER_ADDR=$(cat $WALLET_PATH/$USER.addr)

UTXO_IN=$(get_address_biggest_lovelace $USER_ADDR)

cardano-cli stake-address delegation-certificate \
    --stake-verification-key-file $WALLET_PATH/$USER-stake.vkey \
    --stake-pool-id $2 \
    --out-file $WALLET_PATH/$USER-delegation.cert 

cardano-cli query protocol-parameters \
    --testnet-magic ${TESTNET_MAGIC} \
    --out-file $PROTOCOL

cardano-cli transaction build \
    --testnet-magic ${TESTNET_MAGIC} \
    --change-address $USER_ADDR \
    --tx-in ${UTXO_IN} \
    --tx-in-collateral ${FROM_UTXO} \
    --certificate-file $NETWORK_DIR_PATH/$USER-delegation.cert \
    --protocol-params-file $PROTOCOL \
    --out-file $TX_PATH/delegate.raw

cardano-cli transaction sign \
    --testnet-magic ${TESTNET_MAGIC} \
    --tx-body-file $TX_PATH/delegate.raw \
    --out-file $TX_PATH/delegate.signed \
    --signing-key-file $WALLET_PATH/$USER-stake.skey  \
    --signing-key-file $WALLET_PATH/$USER.skey

cardano-cli transaction submit \
    --testnet-magic ${TESTNET_MAGIC} \
    --tx-file $TX_PATH/delegate.signed
