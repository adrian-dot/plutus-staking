#!/bin/bash

source env.sh
USER=$1

cardano-cli query stake-address-info \
    --testnet-magic ${TESTNET_MAGIC} \
    --address $(cat $WALLET_PATH/$USER-stake.addr)
