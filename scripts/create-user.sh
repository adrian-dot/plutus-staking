#!/bin/bash

. ./env.sh

USER=$1

cardano-cli address key-gen \
    --verification-key-file $WALLET_PATH/$USER.vkey \
    --signing-key-file $WALLET_PATH/$USER.skey

cardano-cli address build \
    --testnet-magic ${TESTNET_MAGIC} \
    --payment-verification-key-file $WALLET_PATH/$USER.vkey \
    --out-file $WALLET_PATH/$1.addr

