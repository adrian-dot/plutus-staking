#!/bin/bash

USER=$1

cardano-cli query utxo \
    --testnet-magic ${TESTNET_MAGIC} \
    --address $(cat ../cardano-private-testnet-setup/private-testnet/addresses/$USER.addr)
