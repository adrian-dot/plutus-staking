#!/bin/bash
# sampel input: ./000-setup.sh

chmod +x 000a-start-private-testnet.sh
chmod +x query-initial-addresses.sh
chmod +x scripts/query-user.sh

cd ..

git clone https://github.com/woofpool/cardano-private-testnet-setup.git

cd cardano-private-testnet-setup
git checkout babbage