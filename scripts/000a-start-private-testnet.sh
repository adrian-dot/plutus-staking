#!/bin/bash

. ./env.sh

if [[ -d "$NETWORK_DIR_PATH" ]]; then
        rm -r "$NETWORK_DIR_PATH"
    fi

    mkdir $NETWORK_DIR_PATH
    mkdir $TX_PATH
    mkdir $WALLET_PATH

cd $REPO_HOME/cardano-private-testnet-setup
./scripts/automate.sh
