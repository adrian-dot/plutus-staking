#!/bin/bash

source env.sh

cardano-cli query stake-address-info \
    --testnet-magic ${TESTNET_MAGIC} \
    --address $(cat $WALLET_PATH/user1-script-stake.addr)
