#!/bin/bash

source env.sh


UTXO_IN=$(get_address_biggest_lovelace $(cat $WALLET_PATH/$1.addr))

txin=$1
amt1=$(./query-stake-address-info-user1-script.sh user1 | jq .[0].rewardAccountBalance)
amt2=$(expr $amt1 / 2 + 1)
raw=$TX_PATH/tx.raw
signed=$TX_PATH/tx.signed

echo "txin = $1"
echo "amt1 = $amt1"
echo "amt2 = $amt2"

cardano-cli query protocol-parameters \
    --testnet-magic ${TESTNET_MAGIC} \
    --out-file $PROTOCOL

cardano-cli transaction build \
    --testnet-magic ${TESTNET_MAGIC} \
    --change-address $(cat $WALLET_PATH/$1.addr) \
    --out-file $raw \
    --tx-in ${UTXO_IN} \
    --tx-in-collateral ${UTXO_IN} \
    --tx-out "$(cat $WALLET_PATH/user2.addr)+$amt1 lovelace" \
    --withdrawal "$(cat $WALLET_PATH/$1-stake.addr)+$amt1" \
    --withdrawal-script-file $WALLET_PATH/stake-validator.script \
    --withdrawal-redeemer-file $NETWORK_DIR_PATH/unit.json \
    --protocol-params-file $PROTOCOL

cardano-cli transaction sign \
    --testnet-magic ${TESTNET_MAGIC} \
    --tx-body-file $raw \
    --out-file $signed \
    --signing-key-file $WALLET_PATH/user1.skey

cardano-cli transaction submit \
    --testnet-magic ${TESTNET_MAGIC} \
    --tx-file $signed
