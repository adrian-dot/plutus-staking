#!/bin/bash


amt=$(./stakeBalance.sh $1 | jq .[0].rewardAccountBalance)

echo "amt = $amt"

source env.sh 

USER=$1
UTXO_IN=$(get_address_biggest_lovelace $(cat $WALLET_PATH/$USER-payment.addr))

cardano-cli transaction build \
    --testnet-magic ${TESTNET_MAGIC} \
    --change-address $(cat $WALLET_PATH/$USER-payment.addr) \
    --out-file $TX_PATH/withdraw.tx \
    --tx-in ${UTXO_IN} \
    --protocol-params-file $PROTOCOL \
    --withdrawal "$(cat $WALLET_PATH/$1-stake.addr)+$amt" \

cardano-cli transaction sign \
    --testnet-magic ${TESTNET_MAGIC} \
    --tx-body-file $TX_PATH/withdraw.tx \
    --out-file $TX_PATH/withdraw.signed \
    --signing-key-file $WALLET_PATH/$1.skey \
    --signing-key-file $WALLET_PATH/$1-stake.skey

cardano-cli transaction submit \
    --testnet-magic ${TESTNET_MAGIC} \
    --tx-file $TX_PATH/withdraw.signed
