#!/bin/bash

export REPO_HOME="$HOME/gimbalabs/plutus-staking" #path to this repository
export CARDANO_NODE_SOCKET_PATH="$REPO_HOME/cardano-private-testnet-setup/private-testnet/node-spo1/node.sock" #path to node socket 
export NETWORK_DIR_PATH="$REPO_HOME/private" # path to network in use (preprod/private)
export TESTNET_MAGIC=42 # magic refeering to network in use (1/42)
export PROTOCOL="$NETWORK_DIR_PATH/protocol.json"

TX_PATH="$NETWORK_DIR_PATH/tx"

##### Users #####

export WALLET_PATH="$NETWORK_DIR_PATH/wallets"

get_address_biggest_lovelace() {
    cardano-cli query utxo --address $1 --testnet-magic ${TESTNET_MAGIC} |
        tail -n +3 |
        awk '{printf "%s#%s %s \n", $1 , $2, $3}' |
        sort -rn -k2 |
        head -n1 |
        awk '{print $1}'
}