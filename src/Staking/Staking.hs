{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Staking.Staking
    ( stakeValidator
    ) where

import Plutus.V1.Ledger.Value
import Plutus.V1.Ledger.Contexts
import Plutus.V1.Ledger.Api
import PlutusTx
import PlutusTx.Prelude hiding (Semigroup (..), unless)

{-# INLINABLE mkStakingValidator #-}
mkStakingValidator :: Address -> () -> ScriptContext -> Bool
mkStakingValidator addr () ctx = case scriptContextPurpose ctx of
    Certifying _   -> True
    Rewarding cred -> traceIfFalse "insufficient reward sharing" $ paidToAddress >= amount cred
    _              -> False
  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    amount :: StakingCredential -> Integer
    amount cred = go $ toList (txInfoWdrl info)
      where
        go :: [(StakingCredential, Integer)] -> Integer
        go [] = traceError "withdrawal not found"
        go ((cred', amt) : xs)
            | cred' == cred = amt
            | otherwise     = go xs

    paidToAddress :: Integer
    paidToAddress = foldl f 0 $ txInfoOutputs info
      where
        f :: Integer -> TxOut -> Integer
        f n o
            | txOutAddress o == addr = n + valueOf (txOutValue o) adaSymbol adaToken 
            | otherwise              = n

{-# INLINEABLE untypedValidator #-}
untypedValidator :: Address -> BuiltinData -> BuiltinData -> ()
untypedValidator params redeemer ctx =
  check
    ( mkStakingValidator
        params
        (PlutusTx.unsafeFromBuiltinData redeemer)
        (PlutusTx.unsafeFromBuiltinData ctx)
    )

stakeValidator :: Address -> StakeValidator
stakeValidator params = mkStakeValidatorScript 
  ($$(PlutusTx.compile [||untypedValidator||])
    `PlutusTx.applyCode` PlutusTx.liftCode params)



--validator :: Address -> Validator 
--validator tp = mkStakingScript (validatorScript tp)


{--
stakeValidator :: Address -> StakeValidator
stakeValidator addr = mkStakeValidatorScript $
    $$(PlutusTx.compile [|| wrapStakeValidator . mkStakingValidator ||])
    `PlutusTx.applyCode`
    PlutusTx.liftCode addr--}
